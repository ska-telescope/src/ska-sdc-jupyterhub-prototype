# Basic storage configuration

## Local storage (for test development only)

First, apply the next configuration:

```
kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
```
Confirm that the new StorageClass is set:

````
kubectl get storageclass
````

Set this StorageClass as default:

````
kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
`````

After that, you have to include in `values.yml` this reference `storageClassName: local-path` of StorageClass for `binderhub.jupyterhub.hub.db.pvc.storageClass: local-path`:

```
binderhub:
  ....
  jupyterhub:
    hub:
        ...
        db:
            pvc:
            storageClassName: local-path
  ....
```

and

```
binderhub:
  ....
  jupyterhub:
    ...
    singleuser:
        ...
        storage:
            dynamic:
                storageClass: local-path
  ....
```

## Cinder storage - OpenStack (for production environments)

Create a new file named `cinder.yaml` and include the next content:

```
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: cinder
  annotations:
    storageclass.beta.kubernetes.io/is-default-class: "true"
  labels:
    kubernetes.io/cluster-service: "true"
    addonmanager.kubernetes.io/mode: EnsureExists
provisioner: kubernetes.io/cinder
parameters:
  availability: nova
```

For this StorageClass, we will user Cinder on OpenStack, and we are indicating that out availability zone is `nova`.

apply the next configuration:

```
kubectl apply -f cinder.yaml
```

Select it as default:

```
kubectl patch storageclass cinder -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
```

Then, you have to include this new StorageClass in your `values.yaml`:

```
binderhub:
  ....
  jupyterhub:
    hub:
        ...
        db:
            pvc:
            storageClassName: cinder
  ....
```

and

```
binderhub:
  ....
  jupyterhub:
    ...
    singleuser:
        ...
        storage:
            dynamic:
                storageClass: cinder
  ....
```
