# Container registry configuration

## Using a public container registry 

Create a new file named `secrets.yaml` and include your credentials for `https://hub.docker.com/`:

```
binderhub:
   registry:
     username: ""
     password: ""
```

Save it, and then you will use it with the Helm deployment.


## Using a private container registry 

For each node within your Kubernetes cluster deployment, run the next:

```
echo '{ "insecure-registries": ["192.168.100.127:30000"] }' > /etc/docker/daemon.json
```

*Note: Change 192.168.100.127 with an IP of your kubernetes master node*

```
systemctl reload docker
```

Then, on the Kubernetes master we set our local registry:

```
docker run -d -p 30000:5000 --restart=always --name registry registry
```

After this steps, you have to push all the images related with Jupyter/Binder, as well as your notebooks containers.

For example for this container registry, first re-tag images:

```
docker tag jupyterhub/k8s-hub:0.10.4 192.168.100.127:30000/jupyterhub/k8s-hub:0.10.4
docker tag jupyterhub/k8s-singleuser-sample:0.10.4 192.168.100.127:30000/jupyterhub/k8s-singleuser-sample:0.10.4
docker tag jupyterhub/k8s-image-awaiter:0.10.4 192.168.100.127:30000/jupyterhub/k8s-image-awaiter:0.10.4
docker tag jupyterhub/k8s-network-tools:0.10.4 192.168.100.127:30000/jupyterhub/k8s-network-tools:0.10.4
docker tag jupyterhub/configurable-http-proxy:4.2.2 192.168.100.127:30000/jupyterhub/configurable-http-proxy:4.2.2
docker tag jupyter/all-spark-notebook 192.168.100.127:30000/jupyter/all-spark-notebook
docker tag jupyter/datascience-notebook 192.168.100.127:30000/jupyter/datascience-notebook
docker tag jupyter/minimal-notebook 192.168.100.127:30000/jupyter/minimal-notebook
docker tag amigahub/jupyter-desktop-server:20201014 192.168.100.127:30000/amigahub/jupyter-desktop-server:20201014
```

Then push the images to the new container registry:

```
docker push 192.168.100.127:30000/jupyterhub/k8s-hub:0.10.4
docker push 192.168.100.127:30000/jupyterhub/k8s-singleuser-sample:0.10.4
docker push 192.168.100.127:30000/jupyterhub/k8s-image-awaiter:0.10.4
docker push 192.168.100.127:30000/jupyterhub/k8s-network-tools:0.10.4
docker push 192.168.100.127:30000/jupyterhub/configurable-http-proxy:4.2.2
docker push 192.168.100.127:30000/jupyter/all-spark-notebook
docker push 192.168.100.127:30000/jupyter/datascience-notebook
docker push 192.168.100.127:30000/jupyter/minimal-notebook
docker push 192.168.100.127:30000/amigahub/jupyter-desktop-server:20201014
```

Once you have this images ready in your private registry, you have to modify all the references in `values.yaml` to the new registry, for example:

```
...
  singleuser:
      cmd: jupyterhub-singleuser
      defaultUrl: /lab
      image:
        name: 192.168.100.127:30000/pangeo/base-notebook
...
```