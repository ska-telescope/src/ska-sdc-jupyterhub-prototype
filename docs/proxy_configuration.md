# Proxy configuration

Since the services exported in Helm are exposed as `LoadBalancer` in Kubernetes, it will be necessary to make the appropriate redirections to those ports.    

You can use Nginx, Apache or another service for this. 

In this example we will configure access from Apache, to provide connectivity to Binder and Jupyter.

```
vi /etc/apache2/sites-available/000-default.conf
```

Note: 
- Our kubernetes Master node IP is the `192.168.100.135`
- `binderhub` service is exported in port `32692`.
- `jupyterhub` or our `daskhub` service is exported in port `31591`.


````
  # BindeHub
  RewriteCond %{HTTP:Connection} Upgrade [NC]
  RewriteCond %{HTTP:Upgrade} websocket [NC]
  ProxyPreserveHost on
  RewriteRule /binderhub/(.*)  http://192.168.100.135:32692/binderhub/$1 [NE,P,L]
  ProxyPass /binderhub/ http://192.168.100.135:32692/binderhub/
  ProxyPassReverse /binderhub/ http://192.168.100.135:32692/binderhub/

  # JupyterHub - DuskHub
  RewriteCond %{HTTP:Connection} Upgrade [NC]
  RewriteCond %{HTTP:Upgrade} websocket [NC]
  ProxyPreserveHost on
  RewriteRule /daskhub/(.*) ws://192.168.100.135:31591/daskhub/$1 [NE,P,L]
  RewriteRule /daskhub/(.*) http://192.168.100.135:31591/daskhub/$1 [NE,P,L]
  ProxyPass   /daskhub/ http://192.168.100.135:31591/daskhub/
  ProxyPassReverse /daskhub/ http://192.168.100.135:31591/daskhub/

````

Then restart apache: `sudo systemctl restart apache2`

After that you will access these services:

- https://<your_external_ip>/binderhub/
- https://<your_external_ip>/daskhub/

