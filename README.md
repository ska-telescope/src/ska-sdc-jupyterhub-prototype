# SKA SDC JupyterHub Prototype

This is a prototype service which builds on the JupyterHub multi-user computing platform. It is designed to be easily installed on Kubernetes with Helm.

In addition to JupyterHub, this chart includes the Binder service, which allows dynamic creation of Jupyter notebook environments based on user-defined requirements, and Dask Gateway, a service for provisioning Dask clusters to handle larger workloads.

## Pre-requisites

- A running Kubernetes cluster with version >= 1.20.
- Available storage connected to Kubernetes. [Go to configuration](./docs/storage_configuration.md).
- A local container registry or use the public DockerHub registry. [Go to configuration](./docs/proxy_configuration.md).
- A proxy configuration for accessing services [Go to configuration](./docs/proxy_configuration.md).

## Installation -- from package

To install the packaged Helm chart from this repository's package registry, you will need to add this repository to Helm:

```
$ helm repo add ska-jhub-repo https://gitlab.com/api/v4/projects/45067233/packages/helm/stable
```

And then install the chart:

```
$ helm install --namespace=<your-namespace> ska-sdc-jupyterhub ska-jhub-repo/ska-sdc-jupyterhub
```

As outlined below, some configuration values should be overridden, for example by passing `--values=config.yaml` to this command.

## Installation - from source

After cloning this repository, from the project root, update the Helm chart dependencies:
```
$ helm dependency update
```

Before installation, a number of configuration settings must be set in the `values.yaml`, or an overriding file called e.g. `config.yaml` or `secret.yaml`. These are noted in the commented lines of `values.yaml`.

### Configure image registry

The Binder service will need an image registry to store created notebook environment images. More information about this can be found at https://binderhub.readthedocs.io/en/latest/zero-to-binderhub/setup-binderhub.html#create-secret-yaml-file.

This should be added to the template block provided in `secret.yaml`.

### Set domain

Depending where the JupyterHub service will be hosted, the URLs in the `values.yaml` will need updating. This includes `binderhub.config.BinderHub.hub_url` - which should point to the JupyterHub's URL.

### Configure Authentication

A template for using the SKA's IAM prototype AAI service to provide authentication is provided in `values.yaml`. A new client may need to be registered to obtain values for the `client_id`, `client_secret`, or contact the repository maintainer for an existing client to use.

The two callback URLs must be set. There is the `binderhub.jupyterhub.hub.config.GenericOAuthenticator.oauth_callback_url` - which should be the JupyterHub's base domain, with `/hub/oauth_callback` appended. Then there is `binderhub.jupyterhub.hub.services.binder.oauth_redirect_uri` which should be set to the Binder service's URL, with `/oauth_callback` appended.

See https://binderhub.readthedocs.io/en/latest/authentication.html for more information.

### Helm install

Once these configuration settings have been added, the services can be installed via:
```
$ helm upgrade --install --namespace=<your-namespace> ska-sdc-jupyterhub . --values values.yaml --values secret.yaml
```
